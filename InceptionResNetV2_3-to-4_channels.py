#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from keras.applications.inception_resnet_v2 import InceptionResNetV2

"""
To get 4channels template:

COMMENT OUT THE 3-CHANNEL SHAPE CHECK

E.g. in 
keras.applications.inception_resnet_v2.InceptionResNetV2.py comment out 

#    input_shape = _obtain_input_shape(
#        input_shape,
#        default_size=299,
#        min_size=75,
#        data_format=backend.image_data_format(),
#        require_flatten=False,
#        weights=weights)

and reload imports (e.g. kill and restart kernel)

This operation would effectively leave 
input_shape = input_shape
and alow user-defined number of channels model to be returned.
"""

# Get model template with 4 channels and randomly initialized weights
irv2_4ch = InceptionResNetV2(
        input_shape = (512,512,4),
        weights=None,
        include_top=False)
irv2_4ch.save('InceptionResNetV2_512_4-chan_random.hdf5')

# Get model template with imagenet weights for further 
# implantation in 4-channel model

irv2_orig = InceptionResNetV2(
        input_shape = (512,512,3),
        weights='imagenet',
        include_top=False)
irv2_orig.save('InceptionResNetV2_512_default.hdf5')



#%% Run from this point if you already have saved generated models

from keras.models import load_model

print('Time to have some tea')
irv2_4ch  = load_model('InceptionResNetV2_512_4-chan_random.hdf5')
irv2_orig = load_model('InceptionResNetV2_512_default.hdf5')
print('Wake up, Alice')


#%% Load imagenet weights into 4-channel template

import numpy as np

# Rewrite function to work with indexes
layers_to_modify = [1] # Turns out the only layer that changes
                                    # shape due to 4th channel is the first
                                    # convolution layer.

for index in range(len(irv2_4ch.layers)):
    
    if irv2_4ch.layers[index].get_weights() != []:
        print('non-emtpy weigths layer found:',index)
        if index in layers_to_modify:
            print("modifying layer",index)
            
            # Save weights in buffer variable
            imagenet_weigths_3ch = irv2_orig.layers[index].get_weights()[0]
            # This does not use bias, so only [0] 
            # where kernels are stored is needed. 
            # Otherwise also add line for biases in [1]
            
            # Modify weights to have extra channel
            
            imagenet_weights_4ch = np.concatenate((imagenet_weigths_3ch,
                                                  imagenet_weigths_3ch[:,:,-1:,:]),
                                                  axis=-2) # For channels_last
            
            irv2_4ch.layers[index].set_weights([imagenet_weights_4ch])
            
        else:
            irv2_4ch.layers[index].set_weights(
                    irv2_orig.layers[index].get_weights())


#%% Save 4 channel model populated with weights for futher use    
    
irv2_4ch.save('InceptionResNetV2_512_4-chan_imagenet.hdf5')
