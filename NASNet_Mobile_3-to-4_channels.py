#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from keras.applications.nasnet import NASNetMobile




nn_orig = NASNetMobile(input_shape=(512,512,3), 
                       include_top=False, 
                       weights='imagenet')
nn_orig.save('nn_orig.hdf5')


nn_4ch = NASNetMobile(input_shape=(512,512,4),
                      include_top=False,
                      weights = None)
nn_4ch.save('nn_4ch-random.hdf5')

#%% Run from this point if you already have saved generated models

from keras.models import load_model

print('Time to have some tea')
nn_orig = load_model('nn_orig.hdf5')
nn_4ch  = load_model('nn_4ch-random.hdf5')
print('Wake up, Alice')


#%% Load imagenet weights into 4-channel template

import numpy as np

# Rewrite function to work with indexes
layers_to_modify = [1] # Turns out the only layer that changes
                                    # shape due to 4th channel is the first
                                    # convolution layer.

for index in range(len(nn_4ch.layers)):
    
    if nn_4ch.layers[index].get_weights() != []:
        print('non-emtpy weigths layer found:',index)
        if index in layers_to_modify:
            print("modifying layer",index)
            
            # Save weights in buffer variable
            imagenet_weigths_3ch = nn_orig.layers[index].get_weights()[0]
            # This does not use bias, so only [0] 
            # where kernels are stored is needed. 
            # Otherwise also add line for biases in [1]
            
            # Modify weights to have extra channel
            
            imagenet_weights_4ch = np.concatenate((imagenet_weigths_3ch,
                                                  imagenet_weigths_3ch[:,:,-1:,:]),
                                                  axis=-2) # For channels_last
            
            nn_4ch.layers[index].set_weights([imagenet_weights_4ch])
            
        else:
            nn_4ch.layers[index].set_weights(
                    nn_orig.layers[index].get_weights())


#%% Save 4 channel model populated with weights for futher use    
    
nn_4ch.save('nn_4ch_imagenet.hdf5')