# Keras pretrained 3 to 4 channels

Extending input dimensions for pretrained network to N+1 channels. 

The .py files contain two examples for different networks. 

The .ipytnb is one of those examples wrapped into Jupyter notebook format.

The .html files are graphs for example neural networks. Just for demonstration that they both start with one conv layer, essentially the only layer that needs to be extended to N+1 channels.
